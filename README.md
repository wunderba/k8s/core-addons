# core-addons

An opinonated set of [flux](https://fluxcd.io) Objects for installing essential ("core") utilities into your Kubernetes cluster. See [below](#add-ons) for full list.



[[_TOC_]]

## Quick-start

See [Quick-start](docs/quickstart.md)

## Variants

Some add-ons may use different providers to fullfil their purpose. For example [cert-manager](add-ons/cert-manager) may use [http](add-ons/cert-manager/http) or dns for validating with Let's Encrypt. DNS in turn may either use an [rfc2136-compatible dns server](add-ons/cert-manager/dns-rfc2136) or [scaleway dns](add-ons/cert-manager/dns-scaleway). Add-ons with variants cannot be included directly, but need to reference the desired variant (e.g. `cert-manager/http`).

Add-ons without variants are simply included by name (e.g. `oidc`).

For example:

```yaml
...
  components:
  - ../cert-manager/http
  - ../ingress-nginx/standard
  - ../external-dns/rfc2136
  - ../oidc
...
```

## <a name="add-ons"></a>Included add-ons

| Name                                                      | Description                                               | Variants                                                                                                                                      | Depends on                        | Source                                                                    |
|-----------------------------------------------------------|-----------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------|---------------------------------------------------------------------------|
| [cert-manager](add-ons/cert-manager)                      | Cloud native certificate management with [Let's Encrypt](https://letsencrypt.org) | [dns-cloudns](add-ons/cert-manager/dns-cloudns)<br/>[dns-rfc2136](add-ons/cert-manager/dns-rfc2136)<br/>[dns-scaleway](add-ons/cert-manager/dns-scaleway)<br/>[http](add-ons/cert-manager/http)   |           | [cert-manager](https://github.com/cert-manager/cert-manager)              |
| [cilium-default-deny](add-ons/cilium-default-deny)        | Secure your cluster by denying all traffic that is not explicitly allowed |                                                                                                                               |                                   |                                                                           |
| [core-sources](add-ons/core-sources)                      | Various helm repos, reused across add-ons                 |                                                                                                                                               |                                   |                                                                           |
| [external-dns](add-ons/external-dns)                      | Synchronize K8S Services and Ingresses with DNS providers | [rfc2136](add-ons/external-dns/rfc2136)<br/>[scaleway](add-ons/external-dns/scaleway)                                                         |                                   | [external-dns](https://github.com/kubernetes-sigs/external-dns)           |
| [external-secrets](add-ons/external-secrets)              | Integrate external secret management systems into K8S     | [generic](add-ons/external-secrets/generic)<br/>[scaleway](add-ons/external-secrets/scaleway)                                                 |                                   | [external-secrets](https://github.com/external-secrets/external-secrets)  |
| [gitlab-webhook](add-ons/gitlab-webhook)                  | Trigger flux-system reconciles on GitLab changes          |                                                                                                                                               |                                   |                                                                           |
| [goldilocks-polaris](add-ons/goldilocks-polaris)          | Display suggestions to improve your pod specs             |                                                                                                                                               |                                   | [Goldilocks](https://github.com/fairwindsOps/goldilocks)<br/>[Polaris](https://github.com/fairwindsOps/polaris) |
| [grafana-alloy](add-ons/grafana-alloy)                    | Push logs and metrics to Grafana compatible sinks         | [scaleway-cockpit](add-ons/grafana-alloy/scaleway-cockpit)                                                                                    | external-secrets<br/>infrastructure-secrets | [grafana alloy](https://github.com/grafana/alloy)               |
| [infrastructure-secrets](add-ons/infrastructure-secrets)  | Platform specific secrets required by other add-ons       | [scaleway](add-ons/infrastructure-secrets/scaleway)                                                                                           |                                   |                                                                           |
| [ingress-nginx](add-ons/ingress-nginx)                    | An Ingress controller for K8S using NGINX as a reverse proxy and load balancer.    | [control-plane](add-ons/ingress-nginx/control-plane)<br/>[standard](add-ons/ingress-nginx/standard)<br/>[scaleway](add-ons/ingress-nginx/scaleway)<br/>[ovh](add-ons/ingress-nginx/ovh) |         | [ingress-nginx](https://github.com/kubernetes/ingress-nginx/)             |
| [kubernetes-dashboard](add-ons/kuberentes-dashboard)      | A general purpose, web-based UI for Kubernetes clusters.  |                                                                                                                                               | oidc                              | [dashboard](https://github.com/kubernetes/dashboard)                      |
| [oidc](add-ons/oidc)                                      | Protect your cluster using various OIDC providers         |                                                                                                                                               | core-sources<br/>external-secrets | [dex](https://github.com/dexidp/dex)<br/>[oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy) |
| [pmm](add-ons/pmm)                                        | Monitor external database instances                       |                                                                                                                                               | core-sources<br/>external-secrets | [pmm](https://github.com/percona/pmm)                                     |
