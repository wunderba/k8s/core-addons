controller:
  enableAnnotationValidations: true
  kind: Deployment
  replicaCount: 2
  image:
    pullPolicy: Always
  allowSnippetAnnotations: true
  watchIngressWithoutClass: true

  metrics:
    enabled: ${enableMetrics:=false}
    serviceMonitor:
      enabled: ${enableMetrics:=false}
      additionalLabels:
        release: prometheus

  service:
    externalTrafficPolicy: ${ingress_external_traffic_policy:-Local}
    type: ${ingress_service_type:-LoadBalancer}
    omitClusterIP: true
    targetPorts:
      http: http
      https: https
    nodePorts:
      http: "30080"
      https: "30443"
  config:
    use-forwarded-headers: "true"
    proxy-buffer-size: "64k"
    proxy-real-ip-cidr: "0.0.0.0/0"
    disable-ipv6-dns: "true"
    annotations-risk-level: Critical
  publishService:
    enabled: true
  resources:
    requests:
      memory: 100Mi
      cpu: 15m
    limits:
      memory: 200Mi

  podSecurityContext:
    runAsNonRoot: true

  containerSecurityContext:
    allowPrivilegeEscalation: false
    capabilities:
      drop:
        - ALL
      add:
        - NET_BIND_SERVICE
        - SYS_CHROOT
    readOnlyRootFilesystem: true
    runAsUser: 101
    runAsGroup: 101
    seccompProfile:
      type: RuntimeDefault

  extraInitContainers:
    - name: copy-config
      command: ['sh', '-c', 'cp -a /etc/nginx/* /mnt/config/ && cp -a /etc/ingress-controller/* /mnt/ingress-controller/ && cp -a /tmp/* /mnt/tmp/']
      image: "${image_registry}/${image_repository}${image_suffix}:${image_tag}"
      imagePullPolicy: Always
      securityContext:
        allowPrivilegeEscalation: false
        capabilities:
          drop:
            - ALL
        readOnlyRootFilesystem: true
        runAsUser: 101
        seccompProfile:
          type: RuntimeDefault
      volumeMounts:
        - name: config
          mountPath: /mnt/config
        - name: ingress-controller
          mountPath: /mnt/ingress-controller
        - name: tmp
          mountPath: /mnt/tmp

  extraVolumeMounts:
    - name: tmp
      mountPath: /tmp
    - name: config
      mountPath: /etc/nginx
    - name: ingress-controller
      mountPath: /etc/ingress-controller
    - name: ajp-temp
      mountPath: /usr/local/nginx/ajp_temp

  extraVolumes:
    - name: tmp
      emptyDir:
        medium: Memory
    - name: config
      emptyDir: {}
    - name: ingress-controller
      emptyDir: {}
    - name: ajp-temp
      emptyDir:
        medium: Memory

  topologySpreadConstraints:
    - maxSkew: 1
      topologyKey: topology.kubernetes.io/zone
      whenUnsatisfiable: DoNotSchedule
      labelSelector:
        matchLabels:
          app.kubernetes.io/name: ingress-nginx
          app.kubernetes.io/component: controller

defaultBackend:
  enabled: false
