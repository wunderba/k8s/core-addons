## 0.18.0 (2025-03-05)

### changed (5 changes)

- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.9](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8b64d1bddf5ed0c50b3f0d66f9ff74c8815d94f0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))
- [chore(deps): pin fluxcd/flux-cli docker tag to 3c9606b](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/806f5319170ff5059986d475d310f3fe26e0e29a) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))
- [chore(deps): update helm release kubernetes-dashboard to v7.10.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/bb3aaac1028bcd111617b2b9bae1c6a005b19d9d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))
- [chore(deps): update registry.gitlab.com/pipeline-components/yamllint docker tag to v0.33.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/97151c7731a1ec3072413efd09dcde634a68e1da) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))
- [chore(deps): update alpine docker tag to v3.21](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/e66c44150e718743a9ff6b044f8a3d80b91681a3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))

### added (1 change)

- [feat: add spegel transparent registry mirror](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/06fba0bd5966063066e40e43c8f8dc6598d3b034) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/59))

## 0.17.2 (2025-02-25)

No changes.

## 0.17.1 (2025-02-25)

### changed (3 changes)

- [chore(deps): update registry.k8s.io/ingress-nginx/controller:v1.12.0 docker digest to e6b8de1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/780203bbef57583829e45272c2f85e81bfa46cb2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/55))
- [chore(deps): update registry.k8s.io/autoscaling/cluster-autoscaler:v1.32.0 docker digest to 656d21d](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9f76412fce47131fdcd4dd8ab9d68809a3892e5c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/53))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook:v0.2.8 docker digest to 3c76c4b](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f6ca66edab9db3064fe6d717021913aab5d85297) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/52))

## 0.17.0 (2025-02-25)

### changed (12 changes)

- [chore(deps): update registry.k8s.io/kustomize/kustomize docker tag to v5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c5f681130bc7f6be326d570ed878c430947ef21e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update dependency cronjob to batch/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f07540b807e5b0dd58020438734fa3425ed3d2c7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): replace k8s.gcr.io/kustomize/kustomize docker tag with...](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f613e49c75b4053dad02caf6a3d6c3350c21eea7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/48))
- [chore(deps): update gitkeeper library](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fa0773a5f791250c000e169e632b9370ff76b405) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release cluster-autoscaler to v9.46.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8b683c8086968d96b4ffb6933b03834e1aba7efc) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release cluster-autoscaler to v9.46.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/581ba340f6115c1fe9f44e439fef374aac523bf9) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release alloy to v0.12.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/cd959ab63a7e15537f072739bbc624b294cdb46d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update busybox:1.37 docker digest to 498a000](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3f639ee9f1801d08635d5766426d3d998c3ce15c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release oauth2-proxy to v6.2.8](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d8cf2be6afa7bb4f6d5028064be104ed43b6f813) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release vpa to v4.7.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/be7797d079032a4eea2591d467f3292f720c73a8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.8](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/18684f9f713767e7a011824612a6c74e6b859f83) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))
- [chore(deps): update helm release opentelemetry-operator to v0.80.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/add8d5558eef4e6a06b2ef04d3269bd941455c7f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/50))

## 0.16.1 (2025-02-18)

### changed (25 changes)

- [chore(deps): update helm release openebs to v4.2.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/136c90e481ab3f8a6dab8919d9ecd56355eae65d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.7](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3208bc0c1e3feea260a55b41cfd87c050d9c8df8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d1465e001e5816f55acbb2b25b6d900f89878311) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release opentelemetry-operator to v0.80.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/44e7135bc48b91a568f035c80062417e9a23f529) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release external-dns to v1.15.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/131b05027697c59e205efd0376afa03b649ae5cf) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/de0d349c1147048322f424e65ff3b9cd019387fd) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release external-secrets to v0.14.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/2b8410117899c29d4a32202668cb931a9c6de7cb) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release cert-manager to v1.17.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/539d691bec48c48e341fab48aa9c5a7299d941a3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release cilium to v1.17.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/23ab049b3e277e6b237b606c9774a2bba1dc045d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update registry.k8s.io/kustomize/kustomize docker tag to v5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ed1921b318331e6a8170f9eda2c0e01803b6e266) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/45))
- [chore(deps): update registry.k8s.io/nginx-slim docker tag to v0.27](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3e2a416e39d2e86401fb6544a353a76a04484332) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update registry.k8s.io/kustomize/kustomize docker tag to v3.10.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b8da8731fee9e79e7d13229387b20ecf8cdee2ba) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update openpolicyagent/opa docker tag to v0.70.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/efbe7f273a1d942cc3a2a1805231d717c377c91c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update nginx docker tag to v1.27.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/144c68f2bff46e30ada01fddf30e863e7ae674d6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release opentelemetry-operator to v0.80.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9ab5c8e2bae6e3acf7d767bcf2ca05f308ef3cfd) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/763f0e7196eba4f0fbeeae3ecce3e1f717e98fd7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update helm release oauth2-proxy to v6.2.7](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b711dd72008a21ba5a450c93db6bfc5e525c2695) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/50af43ab5843e351aa1da758ef1033ed7416ebef) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update busybox docker tag to v1.37](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ce518dde7f49dd6707691fcb6837dac7cd0c1835) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update dependency ingress to networking.k8s.io/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/71f1d71be085370dcd15920061ecece930a9a238) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update dependency flowschema to flowcontrol.apiserver.k8s.io/v1beta2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5d60d39f6b90db435e8f9c30c9b88824cddf6fde) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update dependency deployment to apps/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ebdb0d5cf4bd271e5570477cdbc3d51e77007095) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update dependency csistoragecapacity to storage.k8s.io/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8abfad8834668ad8da5e7b1ed342782ea6f732d9) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): update dependency cronjob to batch/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fa21e27d92406e670c1e7fa6784cddd75aec764f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/46))
- [chore(deps): replace k8s.gcr.io/kustomize/kustomize docker tag with...](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/01e1935f96d6d130c454c6d49c65e9631e0e960c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/43))

## 0.16.0 (2025-02-12)

### changed (9 changes)

- [feat(openebs): upgrade to 4.1.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/44b7c164b49074b53aef47c66f538050b963eede) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [chore(cilium): move talos params to optional cilium-site-values configmap](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ee3ff697cd8629663d69cd626914987b635807d3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b61e4df14fd17ca5cdd17f2b13248a048a92d8f3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a71388073e0eb4edea66bb73bb3a1879a3859b41) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update helm release crossplane to v1.19.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7c80b560a0eb7d0124589864c1ebda1f4fec0809) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update helm release cilium to v1.17.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ce1049dbc718cc2282b2a982d6b0161027c8c5b2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update helm release external-secrets to v0.14.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1424bd557932e3fbb07071c58ce37f703d2569e6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update helm release kubernetes-dashboard to v7.10.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f80ea48bfc61fc71c12d79c5678d1261e9bceb6c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))
- [chore(deps): update helm release tigera-operator to v3.29.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/27ba7cccfdecedd99d5d38bd42abe5b1a102b6fa) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/42))

### added (7 changes)

- [feat(oidc): add variant cluster-ca...](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3f83f1af1fb9922712bc3da97fa7c82effce0a6d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat(external-secrets): allow usage on all namespaces with label](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d09a8ce68b2a73b5795c1bba277826c377b072a7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat: add gatekeeper-ui add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/924e3208abb1c66a943f978ae69d3a8d304482ea) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat: add gatekeeper add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5b1f373c711e0338da254872ba2c05df4f05a645) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat: add k3s-ingress-trafik add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a9f763070aed5e7f34accb2f9298ea95c8947516) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat: add k3-systeem-upgrade-controller add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5d7ae2fca4b19e70d08f035733b4500f74f167e8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))
- [feat: add openebs add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/e6fbdd17f022d05fca33d48e89aa548a851c776f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/41))

## 0.15.2 (2025-02-06)

No changes.

## 0.15.1 (2025-02-06)

### changed (1 change)

- [chore(deps): update calico/apiserver docker tag to v3.29.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5c0b97e80f9b6eb00a47278f2161068b97b564a6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/39))

## 0.15.0 (2025-02-05)

### changed (10 changes)

- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.2.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c29f26f914b261305c6362b0861ce0bd22a3e711) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release oauth2-proxy to v6.2.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/49f2ef1370de7b05dff02f942f09d9665e6e27f8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.1.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/71edba8d02bee4a0fffac1d3e9d02d19cd81e159) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.1.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d2d25a1517e75ce7c2b32d5943f7cd2cf0c153c3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.1.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9dca51ade31ef0c5bb802600dc02f4cb4e4e8ea9) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release external-secrets to v0.14.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8fe3f33770c05cd8e250cf4416e8674923424d5d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release cilium to v1.17.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/662f5ae4d9d41561e2d997a9d6fc27ccbd8c59cc) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release cert-manager to v1.17.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3bf9ace3649a6d18a38d137e920babdc5ca71313) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release ps-operator to v0.8.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/396b210b70969deb2a822042e063d113c88ed566) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))
- [chore(deps): update helm release pmm to v1.4.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7cf80dce6bcc1e89907fee030c36cecdf0dc3e85) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/38))

## 0.14.1 (2025-01-30)

### changed (1 change)

- [fix: do not use dynamic kind for manifests repo](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/62438f0daae73024e580d8282ba6666787b59400) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/37))

## 0.14.0 (2025-01-30)

### changed (3 changes)

- [feat: make source repository kind customizable](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fd43e93e75d21029c4ed25a6afd23140f235d635) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/36))
- [chore(deps): update helm release kubernetes-dashboard to v7.10.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1403c1f8d2b4b9101326706a3ee4b02510089cdd) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/36))
- [chore(deps): update ghcr.io/rwunderer/external-dns-cloudns-webhook docker tag to v0.1.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d5dee0853d9902c56dc53f3c10860d864bd472dc) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/36))

## 0.13.0 (2025-01-30)

### changed (9 changes)

- [chore(cert-manager): remove email address from lets encrypt issuer](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/912c9e900c37d674ff243e042f09870f98b7364d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/34))
- [fix(external-dns): enable external-dns for services as well as ingresses](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c619347861e7059daa8bb64161665788ac986d64) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release external-dns to v1.15.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6aa834076511ef743fba66cfebbcd78be1326150) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release oauth2-proxy to v6.2.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ad0df363c38b880d86cb7ce2736856b6c864fbff) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release opentelemetry-operator to v0.79.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9054b83e4bf8fddadd11d15e93e2cc18741f088c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release alloy to v0.11.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b1341d51e070d0fb6a35558fcb1fee5eb7c0509a) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release cluster-autoscaler to v9.46.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/06ac331b5abde3f9d05e138ea10162014a8a141f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release cilium to v1.16.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8153c369f20c8ee4231d5c69d42ac477f074e3e9) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))
- [chore(deps): update helm release external-secrets to v0.13.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d902f1a3b748ac4c313e9d6fe6edd41c3ab8db14) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/35))

### added (2 changes)

- [feat(cert-manager): add dns-cloudns resolver](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/daadcdc8d565bdd3e58bbff1f37713f7057ddf71) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/34))
- [feat(external-dns): add cloudns webhook provider](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4f5fdcbf02c7e712a4ff37c6b544c761bba229a2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/34))

## 0.12.0 (2025-01-21)

### added (10 changes)

- [feat: add dummy network-policy 'none'](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/afea204624baaef3f5338b4ce2cc22cf8a79c3cd) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add aws-nlb variant to ingress-nginx](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/480a0dcbb6aedc954f830965ac1f397ef5b2c9d4) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add talos-ccm addon for easy update of talos ccm](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/963261bba9699f0f000283892eedcb418e3350f3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add opentelemetry operator and collector incl. BetterStack integration](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f142d24545be728a8d9b6fb70ff9c37145af134c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add cluster-autoscaler for hetzner/hcloud](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/43ae79a4215f424bbe6a943cae0f401db75b0b10) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add zalando postgres-operator](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8904ee7ea67c7428190e48134235cae7fb82c1c2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat(netpol): allow vmagent scraping](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/caa43a00db37a57e2a157605af92714caaf77c1b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add kubelet-serving-cert-approver](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/081e20dad97fa4e6ee84af325a8edd66a4d0bd58) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add hubble-ui](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6bd033313d862b7062a3fad79a4cc121e5c1616f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat: add hcloud-csi with encrypted volumes](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5792dad464aaa3bc0cf26bdebcbd6ea8ff7f627f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))

### changed (42 changes)

- [chore(ingress-nginx): remove cpu limit](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7eb8ac7a6a04b0af428d5366fcbcc836449e1397) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cluster-autoscaler to v9.45.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/88f8a3cd66d01c07764cb2cdc11fad2429aa23ba) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.2.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8012e5769b7bcc535a9623999eeb6f5a66bb89bf) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [feat(ingress-nginx): have ingress-nginx take over ingresses without class](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/de2a795c920befc616930c3fc579f2f9e30f4ce6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cert-manager to v1.16.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4e5ab768aabb582dad67d187e4ea4737db39e500) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.2.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/51381b264e19a2f6ba38dfa11f7ffe49a9ff2a72) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.2.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1032630bacc0c23b7195566e3d42890115367312) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update busybox:1.37 docker digest to a5d0ce4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b58de8fe2f2182b4d1a19208ab03bbd901969dd7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release opentelemetry-operator to v0.78.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9e401319f1720b03243c346f1add6eb9056ca857) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.2.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/288b3bcd5d49cbae1566766e8106b88fd79e39c2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release kubernetes-dashboard to v7.10.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7e187b55fe581bba09d8ade69d651a3760e40956) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(hcloud): combine hetzner cloud controllers into single addon](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/aadb9f2fd107ed11516aac19cb03db83def2c140) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update registry.k8s.io/ingress-nginx/controller docker tag to v1.12.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/938094ab689cab8f5c2a7b6ddc6e2f25142b9036) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update registry.k8s.io/autoscaling/cluster-autoscaler docker tag to v1.32.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5db580fe31d834f6d08f4a94e2fce0aa4926418e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release opentelemetry-operator to v0.78.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/eee11a3a3acad25e846721327f0ff9496125eea5) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release opentelemetry-operator to v0.78.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/283cd764853c48b10180b8a97e848433f49e34c1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release opentelemetry-operator to v0.77.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/adcd8e6be0fc7d426799ffde5e01fe5be4e46c98) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release ingress-nginx to v4.12.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0b0a3cf66b02a3b8eb07c94950acdbff0e333504) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cluster-autoscaler to v9.45.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/5cbed396deaf670e5bb1ebfcefbc51267813143c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cluster-autoscaler to v9.44.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7d7b33fe845dd8fee6dd21a5c50acf7fcc926c35) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cluster-autoscaler to v9.43.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/cd7d10645772d5cd44c692da8e4b54f3da4f35aa) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release external-secrets to v0.12.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f42f515f22d81cc1dd21734fc73b88d0e397d191) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release postgres-operator to v1.14.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6facd57b7ea4039fa420fdaeed06f8179ad8c8d8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release crossplane to v1.18.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6a519df441decb1fd3581fc78e5fe050a06320c7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cilium to v1.16.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a691bf3c4bdd6e9c4afa5c6a66998f44d6bd284e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release metallb to v0.14.9](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/078affdd29ad954aa3455f8d9a750333362beca6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release pmm to v1.3.21](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7d658cd3a38a9370a9f34a1d551ee97962a0494f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update busybox:1.37 docker digest to 2919d01](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b3cff44781f3938f4877dd0be4f9464dd48b55e2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.2.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a568b47785b471b79f7d4a3f985cb183f40e40da) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.1.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/967fb2ada2da2edc79719c6c10b493236361c957) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release alloy to v0.10.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/275fa4a4781fb285a4e8c95f5f2e455e65574420) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release external-secrets to v0.11.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/17764d26499295e8124f862a594f1497ea71ca55) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release crossplane to v1.18.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4dce41e8a6bb8658b42800a0a38a0b35e194e340) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release oauth2-proxy to v6.1.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d285950c595919c8471e24e7d9f31775aec26c70) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release external-secrets to v0.10.7](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/92d4517523765339bfbae544be78f112fff0b302) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release tigera-operator to v3.29.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6181befc5396a30865f8822227d783e71a241667) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update busybox:1.37 docker digest to db142d4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/20f7c8a7088f77f0ec7eea36dc9002d881fb36d5) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update calico/apiserver docker tag to v3.29.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fdd7468ae06cd32ff7a22d2af8dfe4b8d617053a) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release external-secrets to v0.10.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7a175f4c623e3fadd7b684e90b68db06eef7334b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update helm release cert-manager to v1.16.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8aa1ff366915e3759ed47a756e258ec1ed80e5ba) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(deps): update busybox:1.37 docker digest to 5b0f33c](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/632fd314b679258733c84ccabb8550f98134bf22) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))
- [chore(metallb): move metallb to core-addons namespace](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/81be5923a79b5c438926f0b0d21fd4814e3ab4e5) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/32))

## 0.11.0 (2024-11-18)

### changed (52 changes)

- [chore(deps): update helm release alloy to v0.10.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/de7abf19a254fa9f818ae0c43c7d7a00a6110b81) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release oauth2-proxy to v6.0.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ae430e21bb32bc755b7c5e7a764d0b0ef4140914) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release vpa to v4.7.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/55fa00da8d5a1f1b2308bb064db02a6f52528ebb) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release polaris to v5.18.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a9894daba6beb56724666e867fa0b1a2b1e45932) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release crossplane to v1.18.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ace453b7c5bb46a1ff4849e576d7fbe0da9ec0c0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release pmm to v1.3.20](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c5ad1a10cc94bc62089ad57cca9a498fb3e0f1d2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.10.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/64fb3d757c25f54924926eb16b259d269c61ceac) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release pmm to v1.3.19](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1e84fa2e65149ec1b48d09ba4225b779d6bd47e1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release vpa to v4.7.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/615a359c4a896acf869160c924d3a940247ae3a0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update calico/apiserver docker tag to v3.29.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9601eee02b21aa753cbdd55ece5b5b585c3c152b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release tigera-operator to v3.29.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/43da80eb938eab7403d4defe8348496918f236ec) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release pmm to v1.3.18](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/96c18cef3b061e88fed7736bec27dcacf2a44b1b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release crossplane to v1.17.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8c06c5d4b2d8ce96075bf85da98892b7fdb95ed8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release external-secrets to v0.10.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d51a2564ad74997df4a4789104cb1dd82b40afc1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.9.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/2652be954fe74a2e48ad7f57fcff1f536566eb86) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/26))
- [chore(deps): update helm release alloy to v0.9.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8c667993d28ea9e4e54e7d042392ac92ab2adb5d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release scaleway-certmanager-webhook to v0.4.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/203fc6a384feff72aff75ea038e0742b7acd8fd4) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/27))
- [chore(deps): update helm release pmm to v1.3.17](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/bfd9695cfdd92324b1edf16228ed5810fb471497) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release cert-manager to v1.16.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/41a4581b98ee0098b947ab8dc8e75424c276622f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release ingress-nginx to v4.11.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ff4a8c6507ade8aafa2ea0015639aa32d69e5302) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release oauth2-proxy to v6.0.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/57ef2cfb3d1fa0536441c23b25eaa72672adc954) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.8.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6283de9d075b3ed0dad6baca8cfde361f4c237c2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release alloy to v0.9.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fde0d63acd05206be9d356b3ea3ed485a0bd9345) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.7.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ff74167d1021a62642e0746e1b7a461a45790618) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release cert-manager to v1.16.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/cf5d1d38267c924a9b14842539c64438b6cd19ce) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release pmm to v1.3.16](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8f23488b571c534aafed714870e39621e061d114) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release oauth2-proxy to v6.0.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7abb153e8cd0a8768093fc169b3bb7f5a69af58c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release oauth2-proxy to v6.0.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/113f1309f78bb41c7c30648b98ccded06f050171) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release alloy to v0.9.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/67976a3ae41d3cc32d312c569f38a83fede80b86) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update busybox docker tag to v1.37](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6e2dc9695a0d02e4b6ebcd33a2a3e76fa834de81) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release goldilocks to v9.0.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9bab3c4662aa96e660f2a2dd3096543d3c2dad40) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release alloy to v0.8.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/bfc2c4c5eef067d40165160125e0bf2f88e68b2f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release alloy to v0.8.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f3a64dfaaf7f27675bd3d60d28e3eae9166bf97d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release external-secrets to v0.10.4](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6b2b37c42759ef6004794a05623674e78eeba4d7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release pmm to v1.3.15](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a2114f2ff623c83652d5b240d43f9f422c1ea895) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/25))
- [chore(deps): update helm release vpa to v4.6.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a1d105768f4b2aec5ae4328f83f5e4c3e606d2d7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release tigera-operator to v3.28.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/724a70a1ea5f32baa95da7c36e1c5a2b522c8517) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update calico/apiserver docker tag to v3.28.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f89fa9f29e3cac90b4f8cbb8f7f75fcd67c6d857) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.6.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/92df279c3cdb7292349b39f485ef611d82476621) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release kubernetes-dashboard to v7.6.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1a51210a0341012a26f74f0c4dd3c4a650cb12a7) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update busybox:1.36 docker digest to c230832](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ac096b50ef8704448b60cadbc08649310e3c7f30) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/24))
- [feat: allow scaleway secret access for monitoring](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/879418ce2d9bb3620f815faaf0c8a307ba369d4c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release external-dns to v1.15.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c8ef823ffa96a995a0524eab17dbf2c6606d7b7e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release external-secrets to v0.10.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/6f4b14751aaa51bbc5ea4016a3460d5affcfa3f2) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release oauth2-proxy to v6.0.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7d858a4394e2391f89254b66e12950f8579121a0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update dependency helmrepository to source.toolkit.fluxcd.io/v1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/58bb58f95b24c74e12c5b99ec5b4fd5d0e515328) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update dependency helmrelease to helm.toolkit.fluxcd.io/v2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0daf0e7713ad5ecece5ce9710aea7d1feff56fa5) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update helm release tigera-operator to v3.28.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fc2e817ac4010bb3d3dd81822ccf4dcfe32e9b4c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update calico/apiserver docker tag to v3.28.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3a05d49b108b7339961af1e7d584a17b54625483) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(deps): update busybox:1.36 docker digest to 34b191d](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/2bcf1e93c05068494c3aa34f27c89d6ac3379c66) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/23))
- [chore(infrastructure-secrets): rename secret external-dns-rfc2136 to external-dns](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b7459468db3b241d272c24fa3d76dc8a34f45973) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore: add core- prefix to all namespaces](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4e21f53afced65df8f5bb40b42fb48ba15911c8b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))

### added (12 changes)

- [chore: add dashboards to dex client domains](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a464fe6ebbccfe6568f7642dad9c20f4c9029d03) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore: add dashboards to dex client domains](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c0e7b9aceefa5c140e00d14750f01e30575c7269) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/27))
- [feat: add crossplane with sql-provider](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/cdee622da459067835703198a5bc0d0c7854bfac) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add percona-mysql-operator](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/624e989eafa1702074101e61d5fc2e59c403dee6) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: allow custom oidc connector settings for dex](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/303df0e83080bfdf2003ccf8d776ff0be98cb98b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [chore(netpol): implement oidc calico network policies](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/01cebd0ef68af9ad0eb7876dfa4c0d12f5bbda23) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add scaleway and ovh specific configuration for ingress-nginx](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/95ca00e0494311b3e9c64c8ff1ed9cd0e882537e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add calico api server](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a37cb9371c0fa00d4717e1c77b0d4ce8bda8638b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add tigera-operator](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4e94103dc973f1b544eb1b79619d90073726aef0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add calico network policies for cert-manager, external-dns, ingress-nginx, external-secrets](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/20610e48a7fc1c20cab5b4110ebfe9e9bdcf9fe3) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add cloudflare as provider for external-dns](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/be7f9717ee9a970d3bcfc4b978c6ab53e9c92826) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))
- [feat: add optional cni parameter to cluster_vars for network-policies](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ea6617e2a0bf06469ac60996be3efb77e28e8d0c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/29))

## 0.10.0 (2024-09-02)

### changed (16 changes)

- [chore(deps): update busybox:1.36 docker digest to 8274294](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7ef04da067b390901b7afabc0b07fccb7a31daca) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/18))
- [chore(deps): update helm release alloy to v0.7.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ffb6c89b56f704161e20a92dd67e51c661cfb21e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release external-secrets to v0.10.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/609037c39a958895a111fa35a9ec8261c81a6f8b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release external-secrets to v0.10.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/35c3a6c3290529ca4f913220bf4fa16917e58268) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release oauth2-proxy to v6.0.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/e4c3a1397cf7a0f88be9966695b6d4579fd21fe1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release alloy to v0.6.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/772b7bc53aada2886b9223c0db5aec37e58a347d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [fix: add missing rfc2136 secret substitution to external-dns, cert-manager](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/672e0a25c4b1bde0ed5edaa8267557580cea7b44) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release goldilocks to v9](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0931fdbc062eb7d81e3318883950fe1ab8863c64) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/17))
- [chore(deps): update helm release cert-manager to v1.15.3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/2ee1212b63f6c1ebf2c8dc6361ea0f349e0cea18) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release ingress-nginx to v4.11.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ce35c5833e2cc43c6eb10ae4fc47b2c93f032bac) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release oauth2-proxy to v6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0c76a61a6cec469568d0ec4cc2b1f95a06f2c2ed) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/16))
- [chore(deps): update helm release oauth2-proxy to v5.3.12](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/54702f9d7f1d5a7502e9e4f2794cb2d9c6ee03de) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release alloy to v0.6.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d8fe1cf62877cfa4bbcfdc6db22b8ae1cb6194b9) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release external-secrets to v0.10.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a17df963c377b52e97a16e6aa62797b7829b0bcf) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release cert-manager to v1.15.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c0dab5540fc6e2ee2bdee3919d544246d09c444b) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore(deps): update helm release oauth2-proxy to v5.3.11](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0c832ac5cd4dab180e9607f2fdd57f8bfb464f5e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))

### added (4 changes)

- [feat: add network policies for cilium hubble](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/86d3204b66a51beb805786985f4de31171af03eb) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [feat: add network-policies for hetzner cloud components](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/601e898081b91992e4db56b474fe75afa152a118) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [feat: add metallb](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/63190c41dadcae342afc633bdfd1a52f9b5db2fe) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))
- [chore: add kustomze test build for each component/variant](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/17491722358bc20f8817cdbbdd2a110f932a2c12) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/19))

## 0.9.0 (2024-07-24)

### changed (2 changes)

- [chor(external-secrets): refactor base into generic variant](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/987299e6666ee32de8769520034898f87767c038) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/15))
- [chore(deps): update helm release oauth2-proxy to v5.3.10](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/1a6e4f22ed6ef4983b90195cbf3583f154bac989) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/15))

## 0.8.1 (2024-07-24)

### changed (1 change)

- [fix(oidc): remove cpu limits from oauth2-proxy and redis pods](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/4c180d810892de42972bc80c0b4511493bb22b88) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/14))

## 0.8.0 (2024-07-24)

### changed (3 changes)

- [fix: set refreshInterval to 0 in generated secrets](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/070d23cb4f5c9d01e4ea0fdc830462aeda5e842e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/13))
- [chore(deps): update helm release ingress-nginx to v4.11.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7b46b0445080d2f1dda64a6bd8aad24d01f928f8) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/13))
- [chore(deps): update helm release oauth2-proxy to v5.3.9](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/136325f84f28ec41ec18b9a17c53c983dfed09ef) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/13))

### added (2 changes)

- [feat(pmm): add percona monitoring and management add-on](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/d3649b9ee96fd4723e19e8a15059e5eeb342ba99) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/13))
- [feat(security): add cilium-default-deny cluster-wide network policies](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ee4b41f25fd398c465dca394a639d0ef3cb8f313) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/13))

## 0.7.0 (2024-07-17)

### changed (17 changes)

- [chore: refactor infrastructure-secrets/scaleway into external-secrets/scaleway](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ada43a17df9ee6e0cedaa77b6c3c5ff8205a25d1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release alloy to v0.5.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9e0d0d9ab97fa1d31b908dfaaebe9c0cc22dfd14) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release ingress-nginx to v4.11.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/ff8fe72325e336645607d5e97b792680b4e02c08) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release alloy to v0.5.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/b8aa2ffb0aed2b7ea10e08d197ba103ca632b83e) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release ingress-nginx to v4.10.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c70dd443101b975084fa2959e61d68499dbc3b63) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release external-secrets to v0.9.20](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/367f64c9ace163c345f4b4cf6ba881abd01e014d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release oauth2-proxy to v5.3.8](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/61837b33d77018cd9d037790e5387cf8c7e73e37) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release oauth2-proxy to v5.3.7](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/01e996f1148bcd3ef88ca3942ea60bcfeccbd82a) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(grafana-alloy): rename default cm to grafana-alloy-default](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/52c513a44a0c6a9b530e842ea92e9a3957bc1c5f) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release cert-manager to v1.15.1](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/8d783eb885ec6e4499405c13ac850acb278498b1) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release alloy to v0.4.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/49d550e5b75d1e66fb16e7695fba3dac531530c5) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release alloy to v0.3.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/3bb5c56b7902f7ebbe2c852f93e2126451fbd921) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release oauth2-proxy to v5.3.6](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9a531b64f42c542d0d69c74f78ee606fa8d05e5d) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): pin busybox docker tag to 9ae97d3](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a5e0e2d0ef9bb9fe35b8f951165f299ee9fc0e8c) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/11))
- [chore(deps): update helm release oauth2-proxy to v5.3.5](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/0e94dec49d168a93115cb065c2e889f39fbf4217) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release vpa to v4.5.0](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/9569f2d481f2e2363c7b8115d3ee53e2e1e59a55) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [chore(deps): update helm release goldilocks to v8.0.2](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/f472529961ef393e52b5aa386212f99517523987) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))

### added (4 changes)

- [feat(doc): add documentation for grafana-alloy](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/c506b182aa3e389f0e0cdd94e7ceb1c54539dfa0) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [feat: add oci release package](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/7981910c17ac11dfe1a0030939605bee39ffc274) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [feat(doc): add first rudimentary README](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/a1b6fda1afaf659f5f6334d2a155fea61240a439) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))
- [feat(grafana): Add grafana-alloy](https://git.caprisys.at/wunderba/k8s/core-addons/-/commit/fc6192ea7cba30b39362659b96966db4d1ec6f16) ([merge request](https://git.caprisys.at/wunderba/k8s/core-addons/-/merge_requests/12))

## 0.6.1 (2024-06-12)

### changed (2 changes)

- [fix(oidc): Limit symbols for redis password](wunderba/k8s/core-addons@a0287a1c0a1ce1bac72445be6c34c38040c9a6f8) ([merge request](wunderba/k8s/core-addons!10))
- [fix(oidc): Fix reference to random redis password in ExternalSecret](wunderba/k8s/core-addons@ef00f8b97eb7665ac6e66b2d00579386651fbc51) ([merge request](wunderba/k8s/core-addons!10))

## 0.6.0 (2024-06-12)

### changed (7 changes)

- [fix(oidc): Reduce oauth2-proxy cookie lifetime](wunderba/k8s/core-addons@c004bffe40f5d96ceb9bdcaf0ccbab06b9f8a2b1) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release external-dns to v1.14.5](wunderba/k8s/core-addons@9078ee37735a62e7c275914beeff72f2164987f5) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release oauth2-proxy to v5.3.4](wunderba/k8s/core-addons@897246cb06cf460d949bafa45713064f52b29d29) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release oauth2-proxy to v5.3.3](wunderba/k8s/core-addons@59da93308c1b5d44bda7c5505395c4b36d78d534) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release cert-manager to v1.15.0](wunderba/k8s/core-addons@1cfdfd2e6384e1f1ff2b7f7d3f65394d0e19ec98) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release external-secrets to v0.9.19](wunderba/k8s/core-addons@746115091a1fedffd06dd5e84877ee928b21de0e) ([merge request](wunderba/k8s/core-addons!9))
- [chore(deps): update helm release kubernetes-dashboard to v7.5.0](wunderba/k8s/core-addons@bd52a5c9edfb8d2b51b7cdbb7e59b15df8b46fc2) ([merge request](wunderba/k8s/core-addons!9))

## 0.5.2 (2024-06-04)

### changed (1 change)

- [fix: Remove dependency on infrastructure-secrets from goldilocks and polaris](wunderba/k8s/core-addons@31d9223d1aaa1066f41c8a092972f3fb00759549) ([merge request](wunderba/k8s/core-addons!8))

## 0.5.1 (2024-06-03)

### changed (1 change)

- [chore(deps): update helm release oauth2-proxy to v5.3.0](wunderba/k8s/core-addons@17f491f4f313b913a38a684c1d6d73a5d6a713ae) ([merge request](wunderba/k8s/core-addons!7))

## 0.5.0 (2024-05-27)

### changed (4 changes)

- [chore(flux): Promote flux HelmRelease and HelmRepository api versions](wunderba/k8s/core-addons@5f4e7ee331ad5ce80248eb484095c94d1a19f90c) ([merge request](wunderba/k8s/core-addons!6))
- [chore(deps): update helm release oauth2-proxy to v5.2.0](wunderba/k8s/core-addons@6b2748e1d50903b40a3ca355802b27ec937949e0) ([merge request](wunderba/k8s/core-addons!5))
- [chore(deps): update helm release oauth2-proxy to v5.1.1](wunderba/k8s/core-addons@2bd926516e78c11981b39b1070272327457f953b) ([merge request](wunderba/k8s/core-addons!6))
- [chore(deps): update helm release oauth2-proxy to v5.1.0](wunderba/k8s/core-addons@ba959c75d9dcc0d5d65c33a9b847065b7396e206) ([merge request](wunderba/k8s/core-addons!6))

## 0.4.0 (2024-05-21)

### changed (7 changes)

- [Use restricted securityContext for scaleway-certmanager-webhook](wunderba/k8s/core-addons@4e08f559598c29e628a2c3de099c374dfa0b2f0e) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release scaleway-certmanager-webhook to v0.3.0](wunderba/k8s/core-addons@13d5822301656dcec8a1315c825445afce84b7da) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release kubernetes-dashboard to v7.4.0](wunderba/k8s/core-addons@8d1465c679dd693cbefd147b17c043f7482d5d70) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release oauth2-proxy to v5.0.7](wunderba/k8s/core-addons@553d7050f354cb870655033aaf3db65fffdf6443) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release ingress-nginx to v4.10.1](wunderba/k8s/core-addons@7f391dbeb568c6b29a1b5d5d14eff5547b1d725a) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release external-secrets to v0.9.18](wunderba/k8s/core-addons@4c329994f19c52e72504605246eb83174335b605) ([merge request](wunderba/k8s/core-addons!4))
- [chore(deps): update helm release cert-manager to v1.14.5](wunderba/k8s/core-addons@5e4e6da7159ae0b334e983dd858b10a4078a9883) ([merge request](wunderba/k8s/core-addons!4))

## 0.3.0 (2024-05-17)

### changed (8 changes)

- [fix: Allow worldwide dns access for cert-manager for proagation checks](wunderba/k8s/core-addons@c40630439600e381ad1d2246c488762f47d0f4e5) ([merge request](wunderba/k8s/core-addons!3))
- [fix: Allow acme api access for cert-manager-controller](wunderba/k8s/core-addons@41ab673fe455854b172e9adc95e3e4088d01939a) ([merge request](wunderba/k8s/core-addons!3))
- [fix: Allow kubernetes-api access for scaleway-certmanager-webhook](wunderba/k8s/core-addons@84c2d0d280e04a2fb384037423b79cd5c7db2c77) ([merge request](wunderba/k8s/core-addons!3))
- [chore: Lessen namespace security for cert-manager with scaleway](wunderba/k8s/core-addons@87b6a98cf2e79ff357712c51005e5e92fc9ee7a7) ([merge request](wunderba/k8s/core-addons!3))
- [fix: Add txtPrefix to external-dns for proper CNAME handling](wunderba/k8s/core-addons@e990cb815532a3059483aff4356d74cf474d13de) ([merge request](wunderba/k8s/core-addons!3))
- [chore: Separate external-dns network policies by provider](wunderba/k8s/core-addons@dcb1fb9c00eb5e7ac1fd0896e8674f321b42ea2f) ([merge request](wunderba/k8s/core-addons!3))
- [chore: Convert all addons to kustomize Components](wunderba/k8s/core-addons@df9f75df41533cad31dfc39612e99a5339b3e277) ([merge request](wunderba/k8s/core-addons!3))
- [chore: Re-factor addon variants into subdirectory structure](wunderba/k8s/core-addons@f7d9a4598a570c00261609a0a7c2877cf342c4da) ([merge request](wunderba/k8s/core-addons!3))

### added (1 change)

- [feat: Add dns-scaleway cert-manager resolver](wunderba/k8s/core-addons@5610c5e1f1266d4805aedd3ca93853c7c13d01d3) ([merge request](wunderba/k8s/core-addons!3))

## 0.2.0 (2024-05-13)

### added (1 change)

- [Changelog](wunderba/k8s/core-addons@b98900bdf0368d0fc93076f4b9e70817d0e0f1a4) ([merge request](wunderba/k8s/core-addons!2))

## 0.1.0 (2024-05-13)

### added (1 change)

- [Initial pre-release](wunderba/k8s/core-addons@b98900bdf0368d0fc93076f4b9e70817d0e0f1a4) ([merge request](wunderba/k8s/core-addons!2))
