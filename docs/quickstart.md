## Quick-start

[[_TOC_]]

### Prerequisites

#### A working flux installation

This repo contains mostly [flux](https://fluxcd.io) manifests.

#### A ConfigMap describing your cluster

for example:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: cluster-vars
  namespace: flux-system
data:
  cluster_domain: my-cluster.example.com
  cluster_issuer: letsencrypt-prod
  cluster_name: my-cluster
  dns_zone: example.com
  external_mysql_cidr: 172.16.8.5/32
  external_pg_cidr: 172.16.8.0/22
  ingress_class: nginx
  ingress_name: ingress-nginx
  ingress_namespace: core-ingress-nginx
  local_issuer: my-cluster-ca
  management_cidr: 0.0.0.0/0
  management_cidr_array: '["0.0.0.0/0"]'
```

### Add core-addons to your flux setup

#### GitRepository

```yaml
---
apiVersion: source.toolkit.fluxcd.io/v1
kind: GitRepository
metadata:
  name: core-addons
  namespace: flux-system
spec:
  url: https://gitlab.com/wunderba/k8s/core-addons.git
  ref:
    tag: 0.7.0
```

#### Selecting the installed add-ons

Create a [Kustomization](https://fluxcd.io/flux/components/kustomize/kustomizations/) object referring to the above [GitRepository](https://fluxcd.io/flux/components/source/gitrepositories/)

* set the kustomization `path` to `./add-ons/core-sources`
* include the desired add-ons as [components](https://fluxcd.io/flux/components/kustomize/kustomizations/#components)
* reference your `cluster-vars` config map in [postBuild](https://fluxcd.io/flux/components/kustomize/kustomizations/#post-build-variable-substitution)

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  path: ./add-ons/core-sources

  components:
  - ../cert-manager/http
  - ../ingress-nginx/standard
  - ../external-dns/rfc2136
  - ../oidc
  - ../kubernetes-dashboard

  serviceAccountName: kustomize-controller
  prune: true

  sourceRef:
    kind: GitRepository
    name: core-addons

  postBuild:
    substituteFrom:
    - kind: ConfigMap
      name: cluster-vars
```
