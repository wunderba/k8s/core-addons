SUBDIRS := $(dir $(shell find -name kustomization.yaml -not -path "./resources/gatekeeper-templates/library/*"))

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {sub("\\\\n",sprintf("\n%22c"," "), $$2);printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: all
all: verify-kustomization test-components yamllint		## Run verify-kustomization, test-components and yamllint

.PHONY: verify-kustomization
verify-kustomization: $(SUBDIRS)						## Make sure all kustomization.yaml's include all files in the same directory

.PHONY: yamllint
yamllint:							    				## Check all yaml files for proper formatting
	@yamllint .

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	@cd $@ && for f in *.yaml; do [ "$$f" = "kustomization.yaml" ] && continue; grep -q "$$f" kustomization.yaml || echo "$$f missing in kustomization.yaml in $@"; done
	@cd $@ && kustomize build -o /dev/null

.PHONY: test-components
test-components:										## Make sure all add-ons are properly configured as components
	./tools/test-components.sh

# vim: syntax=make noexpandtab
