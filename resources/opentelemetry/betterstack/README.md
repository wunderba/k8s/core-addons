# opentelemetry/betterstack add-on

Push OpenTelemetry data (metrics, logs and traces) to Betterstack.

[[_TOC_]]

## Usage

### Prerequisites

* [external-secrets/scaleway](../../external-secrets/scaleway)
* One scaleway secret (in Secret Manager) for pushing OpenTelemetry data repectively

#### Scaleway secrets

The add-on expects one secret in Scaleway's Secret Manager in the same region as the cluster at the following paths. 
`${cluster_name}` is the cluster_name as specified in the `cluster-vars` ConfigMap:

`/k8s/${cluster_name}/${cluster_name}-opentelemetry`:

```json
{
  "OTLP_ENDPOINT_METRICS": "https://in-otel.logs.betterstack.com/metrics",
  "OTLP_ENDPOINT_LOGS": "https://in-otel.logs.betterstack.com",
  "BETTERSTACK_SOURCE_TOKEN": "SomeBetterStackSourceToken"
}
```
