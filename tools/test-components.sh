#!/bin/sh
set -eu

COMPONENTS=$(
    ls -1 add-ons/*/kustomization.yaml add-ons/*/*/kustomization.yaml \
        | grep -Ev "(base|core-sources)" \
        | sed 's#^\(.*\)/kustomization.yaml#../\1#'
)

TESTDIR=$(mktemp -dp .)

for c in ${COMPONENTS}; do
    echo $c

    cat > "${TESTDIR}/kustomization.yaml" <<_EOF_
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../add-ons/core-sources
components:
- ${c}
_EOF_

    kustomize build "${TESTDIR}"
done

rm -rf ${TESTDIR}
