/^apiVersion: source.toolkit.fluxcd.io\/v1beta/ {
    N;
    /kind: HelmRepository/ {
        s#/v1beta[12]#/v1#;
        p;
        d;
    }
}
/^apiVersion: helm.toolkit.fluxcd.io\/v2beta/ {
    N;
    /kind: HelmRelease/ {
        s#/v2beta[12]#/v2#;
        p;
        d;
    }
}
/^apiVersion: helm.toolkit.fluxcd.io\/v1beta1/ {
    N;
    /kind: HelmChart/ {
        s#/v1beta[12]#/v1#;
        p;
        d;
    }
}
p;
