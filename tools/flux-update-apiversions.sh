#!/bin/bash
set -euo pipefail

dir=$(dirname $0)
sed -n -i -f "${dir}/flux.sed" $(grep -rEl "^kind: (GitRepository|Kustomization|Receiver)" .)
sed -n -i -f "${dir}/flux-2.3.sed" $(grep -rEl "^kind: (HelmRepository|HelmChart|HelmRelease)" .)
