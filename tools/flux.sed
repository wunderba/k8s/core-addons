/^apiVersion: source.toolkit.fluxcd.io\/v1beta/ {
    N;
    /kind: GitRepository/ {
        s#/v1beta[12]#/v1#;
        p;
        d;
    }
}
/^apiVersion: kustomize.toolkit.fluxcd.io\/v1beta/ {
    N;
    /kind: Kustomization/ {
        s#/v1beta[12]#/v1#;
        p;
        d;
    }
}
/^apiVersion: notification.toolkit.fluxcd.io\/v1beta1/ {
    N;
    /kind: Receiver/ {
        s#/v1beta[12]#/v1#;
        p;
        d;
    }
}
p;
