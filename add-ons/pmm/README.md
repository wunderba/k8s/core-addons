# pmm

Add [Percona Monitoring and Management](https://docs.percona.com/percona-monitoring-and-management/) to your cluster.
The main use case is to monitor MySQL or PostgreSQL instances external to your cluster.

[[_TOC_]]

## Usage

### Prerequisites

PMM needs to be allowed to talk to your external database. For this you need to specifiy `external_mysql_cidr` and/or
`external_pg_cidr` in your [Cluster Config Map](https://gitlab.com/wunderba/k8s/core-addons/-/blob/develop/docs/quickstart.md#a-configmap-describing-your-cluster) accordingly:

```yaml
...
  external_mysql_cidr: 172.16.8.5/32
  external_pg_cidr: 0.0.0.0/32
...
```

If you need to monitor only a single host, specify its ip address directly. If you need to monitor multiple hosts, specify the smallest network cidr encompassing all hosts.
`0.0.0.0/32` means "no valid ip" and basically blocks access to this type of instance.

### Installation

Include `pmm` in your add-ons config.

### Set up

Once pmm is up and running, navigate to `https://pmm.${cluster_domain}` and log in as user `admin` with the password obtained by

```
kubectl -n pmm get secret pmm-admin-password -o jsonpath="{.data.PMM_ADMIN_PASSWORD}" | base64 -d ; echo
```

Navigate to `PMM Inventory` (you can find it in the gear-wheel menu on the bottom left) and use `Add service` to add your external servers.

You might need to create a dedicated account for pmm, see the following urls for details on the privileges needed:
* [MySQL user](https://docs.percona.com/percona-monitoring-and-management/setting-up/client/mysql.html#create-a-database-account-for-pmm)
* [PostgreSQL user](https://docs.percona.com/percona-monitoring-and-management/setting-up/client/postgresql.html#create-a-database-account-for-pmm)

## OIDC login

For regular usage, use OIDC to login via [dex](../oidc) running in your cluster.
