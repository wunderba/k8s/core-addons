# cert-manager/dns-rfc2136 add-on

Configure the Let's Encrypt ClusterIssuers to use the DNS-01 solver, utilizing the
internal [rfc2136](https://cert-manager.io/docs/configuration/acme/dns01/rfc2136/) provider.

This variant expects a secret named `external-dns` to exist in the `flux-system` namespace.

This secret needs to define at least the following fields:
* `dns_server`: hostname or ip address of an rfc2136-compatible dns server hosting the `${cluster_domain}` zone
* `dns_keyname`: name of the key to use for writing to the zone
* `dns_secret`: secret corresponding to `dns_keyname`

The variant also configures a network policy to allow access from cert-manager
to the configured `${dns_server}` at port 53 (TCP and UDP).

## Usage

Include `cert-manager/dns-rfc2136` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../cert-manager/dns-rfc2136
```
