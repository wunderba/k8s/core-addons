# cert-manager/http add-on

Configure the Let's Encrypt ClusterIssuers to use the HTTP-01 solver. For this to work you
will also need to properly set up the [ingress-nginx](../../ingress-nginx) add-on.

The variant also configures a clusterwide network policy to allow access from the ingress-controller
to all pods labeled `acme.cert-manager.io/http01-solver: "true"` in all namespaces.

## Usage

Include `cert-manager/http` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../cert-manager/http
```
