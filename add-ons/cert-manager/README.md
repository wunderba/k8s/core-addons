# cert-manager add-on

Install and configure [cert-manager](https://github.com/cert-manager/cert-manager).

The add-on will create four ClusterIssuers:

* `${cluster_name}-ca`: ca for all kinds of internal certificates, where no public root ca is needed
* `selfsigned`: helper ca for creating `${cluster_name}-ca`
* `letsencrypt-staging`: Let's Encrypt issuer for testing the certificate infrastructure
* `letsencrypt-prod`: rate limited Let's Encrypt issuer for actual, real certificates

## Variants

The variant selected defines how Let's Encrypt should verify the certificates issued
(see [cert-manager documentation](https://cert-manager.io/docs/configuration/acme/) for details):

* [dns-cloudns](dns-cloudns/): DNS-01 verification with [ClouDNS](https://cloudns.net)
* [dns-rfc2136](dns-rfc2136/): DNS-01 verification with a custom dns server supporting [rfc2136](https://cert-manager.io/docs/configuration/acme/dns01/rfc2136/) updates (e.g. [bind](https://www.isc.org/bind/))
* [dns-scaleway](dns-scaleway/): DNS-01 verification with [Scaleway](https://scaleway.com)
* [http](http/): HTTP-01 verification by creating a special pod in the cluster

HTTP verification will work regardless of dns provider, however it is sometimes a bit tricky
to get right (it needs a working ingress controller for http traffic as well as network
policy allowing entry into the cluster).

DNS verification on the other hand needs one of the supported DNS provider, but will "just work"
if given working credentials.
