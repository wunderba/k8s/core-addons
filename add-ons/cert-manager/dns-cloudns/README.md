# cert-manager/dns-cloudns add-on

Configure the Let's Encrypt ClusterIssuers to use the DNS-01 solver, utilizing the
[cloudns webhook](https://github.com/mschirrmeister/cert-manager-webhook-cloudns) provider.

The webhook will be installed using the [cert-manager-webhook-cloudns](https://github.com/caprisys/helm-charts/tree/main/charts/cert-manager-webhook-cloudns) chart.

This variant expects the `${cluster_domain}` to be properly configured at [ClouDNS](https://cloudns.net).
For access to the domain a secret named `cloudns-config` with the fields
`CLOUDNS_AUTH_ID_TYPE`, `CLOUDNS_AUTH_ID` and `CLOUDNS_AUTH_PASSWORD`
needs to exist in the `core-external-dns` namespace.

The variant also configures a network policy to allow access from the webhook
to [api.cloudns.net](https://api.cloudns.net).

## Usage

Include `cert-manager/dns-cloudns` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../cert-manager/dns-cloudns
```
