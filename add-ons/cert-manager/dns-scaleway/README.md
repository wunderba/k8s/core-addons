# cert-manager/dns-scaleway add-on

Configure the Let's Encrypt ClusterIssuers to use the DNS-01 solver, utilizing the
[scaleway webhook](https://github.com/scaleway/cert-manager-webhook-scaleway) provider.

The webhook will be installed using the [scaleway-certmanager-webhook](https://github.com/scaleway/helm-charts/tree/master/charts/scaleway-certmanager-webhook) chart.

This variant expects the `${cluster_domain}` to be properly configured at Scaleway.
For access to the domain a secret named `external-dns-scaleway` with credentials
in the fields `access_key` and `secret_key` needs to exist in the `core-external-dns`
namespace.

The variant also configures a network policy to allow access from the webhook
to [api.scaleway.com](https://api.scaleway.com).

## Usage

Include `cert-manager/dns-scaleway` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../cert-manager/dns-scaleway
```
