# grafana-alloy add-on

Push logs and metrics to Grafana compatible sinks

## Variants / Sinks

* [grafana-alloy/scaleway-cockpit](scaleway-cockpit/)
* [grafana-alloy/betterstack](betterstack/)
