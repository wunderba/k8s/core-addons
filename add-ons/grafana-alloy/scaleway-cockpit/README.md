# grafana-alloy/scaleway-cockpit add-on

Push logs and metrics to Scaleway cockpit

[[_TOC_]]

## Usage

### Prerequisites

* [external-secrets/scaleway](../../external-secrets/scaleway)
* Two scaleway secrets (in Secret Manager) for pushing logs and metrics repectively

#### Scaleway secrets

The add-on expects two secrets in Scaleway's Secret Manager in the same region as the cluster at the following paths. 
`${cluster_name}` is the cluster_name as specified in the `cluster-vars` ConfigMap:

`/k8s/${cluster_name}/${cluster_name}-log-writer`:

```json
{
  "SCW_COCKPIT_LOG_WRITER_KEY": "SomeKey",
  "SCW_COCKPIT_LOG_WRITER_URL": "https://logs.cockpit.fr-par.scw.cloud/loki/api/v1/push"}
}
```

`/k8s/${cluster_name}/${cluster_name}-metrics-writer`:

```json
{
  "SCW_COCKPIT_METRICS_WRITER_KEY": "SomeKey",
  "SCW_COCKPIT_METRICS_WRITER_URL": "https://metrics.cockpit.fr-par.scw.cloud/api/v1/push"
}
```

## Customization

By default the add-on creates a ConfigMap called `grafana-alloy-default` that
will push logs and metrics of all pods in all namespaces to Scaleway Cockpit.

This might get very expensive pretty soon, so you will want to use a custom
configuration instead.

To that end you will need to:

1. create a custom ConfigMap and values override in your `flux-system` repository
2. include these custom files in your flux kustomization

### Create a custom ConfigMap and values override

Create a new directory `grafana-alloy` at the root of your `flux-system` repository.
This directory will hold the following files:

```
custom-configmap.yaml
custom-values.yaml
kustomization.yaml
namespace.yaml
```

#### `custom-configmap.yaml`

In this file create your custom configuration for Grafana Alloy.

_**Note:** You can choose any name for the ConfigMap and the
key (`config.river`)._

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-alloy-custom
  namespace: core-grafana-alloy
data:
  config.river: |-
    logging {
      level  = "info"
      format = "json"
    }
...
```

#### `custom-values.yaml`

As with any add-on it is possible to partially override
the `values` given to helm with custom parameters.

In our case all that is needed is to set the name of
the ConfigMap and the key we just defined:

_**Note:** The name of this config map must be exactly as shown below:_

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-alloy-custom-values
  namespace: flux-system
data:
  values.yaml: |-
    alloy:
      configMap:
        create: false
        name: "grafana-alloy-custom"
        key: "config.river"
```

#### `kustomization.yaml`

A simple Kustomization file, referencing all the files in this directory:

```yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- namespace.yaml
- custom-configmap.yaml
- custom-values.yaml
```

#### `namespace.yaml`

This file is only needed to make sure that the namespace exists when flux
applies our configuration.

_**Background:** We have a chicken-and-egg situation here:
On the first run flux will try to apply these files before installing
any of the add-ons (including grafana-alloy). For that reason the
namespace will not exists and flux will fail, never getting around to 
installing the add-ons and the namespace._

```yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: grafana-alloy
```

### Include your customizations in flux

Include the new created directory in your cluster's main `kustomization.yaml`:

```yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- gotk-components.yaml
- gotk-sync.yaml
- core-addons.yaml
- ../../../grafana-alloy
patches:
...
```

_**Note:** You can customize the configuration of any add-on this way: if a
ConfigMap with the name_ (add-on-name)-custom-value _exists in the 
`flux-system` namespace, it will be included after the standard add-on
values configuration. Any key from the custom ConfigMap will override the
default value._
