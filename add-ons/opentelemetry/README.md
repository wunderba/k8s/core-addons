# opentelemetry add-on

Push OpenTelemetry data (metrics, logs and traces) to compatible backends.

## Backends

* [opentelemetry/betterstack](betterstack/)
