# cilium-default-deny

Add cilium [ClusterWideNetworkPolicies](https://docs.cilium.io/en/stable/security/policy/kubernetes/#clusterwide-policies) that
will deny all traffic (to/from the cluster and also between pods) that has not been explictly allowed.

[[_TOC_]]

In addition to denying all traffic by default, this add-on will install the following policies, allowing traffic vital for
a functioning cluster:

* **core-dns**: allow all pods to query dns
* **cilium-health-checks**: allow cilium-internal health checks
* **kube-api-server**: allow access from system namespace to the Kubernetes API server and also allow the API server to talk to the kubelet. All other pods that need to talk to the Kubernetes API will have to have their own network policies allowing that traffic
* **kube-etc**: allow direct access to etcd from control-plane nodes only
* **kubelet**: grant kubelet access only to api-server and metrics server, allow the kubelet to talk to the Kubernetes API
* **kube-metrics-server**: allow the metric-server to talk to the Kubernetes API and kubelet

## Usage

Just include `cilium-default-deny` in your list of enabled add-ons.

## Tips and best practices for Network Policies

1. use only a single kind of NetworkPolicy. If your cluster runs Cilium, use `CiliumNetworkPolicy` (and `CiliumClusterwideNetworkPolicy` when necessary).

2. use a CiliumNetworkPolicy per Pod defined in a single file (see below for an example)

3. Create and verify your policies using the [Network Policy Editor](https://editor.networkpolicy.io/)

4. To debug network policies use [hubble](https://github.com/cilium/hubble).

5. If hubble is not available (e.g. at Scaleway):

* find out which node the workload you are trying to debug is running on
* use `kubectl -n kube-system get pods` to find the corresponding cilium pod
* `kubectl exec` into this pod and run `cilium-dbg- monitor --type drop` to watch for dropped packages
* (Drops related to ipv6 `RouterSolicitation` can be ignored)

For more information see: [Cilium Troubleshooting](https://docs.cilium.io/en/latest/operations/troubleshooting)

### Sample policy for a single pod

Suppose the application `my-app` provides a web interface and needs to access a mysql
database server located outside the cluster:

```yaml
---
apiVersion: cilium.io/v2
kind: CiliumNetworkPolicy
metadata:
  name: my-app
  namespace: my-app-test
  labels:
    app.kubernetes.io/name=my-app
spec:
  endpointSelector:
    matchLabels:
      app.kubernetes.io/name=my-app

  ingress:

  # Allow http from ingress-controller
  - fromEndpoints:
    - matchLabels:
        k8s:io.kubernetes.pod.namespace: ${ingress_namespace}
        app.kubernetes.io/name: ${ingress_name}
    toPorts:
    - ports:
      - port: "8080"

  egress:

  # Allow access to managed mysql
  - toCIDR:
    - 172.16.8.250/32
    toPorts:
    - ports:
      - port: "3306"
        protocol: TCP
```
