# ingress-nginx/standard add-on

Install [Ingress NGINX Controller](https://github.com/kubernetes/ingress-nginx) and configure it
for generic use.

This will configure ingress-nginx without any cloud specific configuration.

## Usage

Simply include `ingress-nginx/standard` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../ingress-nginx/standard
```
