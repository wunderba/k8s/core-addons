# ingress-nginx/control-plane add-on

Install [Ingress NGINX Controller](https://github.com/kubernetes/ingress-nginx) and configure it
to run on the control-plane.

This will configure ingress-nginx to run as a `DaemonSet` on the control-plane nodes. The idea is to
combine this with a floating ip and [metallb](../../metallb) to use ingress-nginx without a cloud
load balancer.

## Usage

Simply include `ingress-nginx/control-plane` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../ingress-nginx/control-plane
```
