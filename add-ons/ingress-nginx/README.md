# ingress-nginx add-on

Configure the [ingress-nginx](https://github.com/kubernetes/ingress-nginx) ingress controller.

## Variants

* [standard](standard/)
* [control-plane](control-plane/)
* [scaleway](scaleway/)
* [ovh](ovh/)
