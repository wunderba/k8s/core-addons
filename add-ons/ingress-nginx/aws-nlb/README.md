# ingress-nginx/aws-nlb add-on

Install [Ingress NGINX Controller](https://github.com/kubernetes/ingress-nginx) and configure it
for using an NLB on AWS.

This will add the 

```yaml
service.beta.kubernetes.io/aws-load-balancer-backend-protocol: "tls"
service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout: '3600'
service.beta.kubernetes.io/aws-load-balancer-type: nlb
```

annotations to the ingress `Service` object and configure ingress-nginx for redirecting http
traffic to https:

```yaml
config:
  http-snippet: |
    server {
      listen 8000 ;
      server_tokens off;
      return 301 https://$host$request_uri;
```

## Usage

Simply include `ingress-nginx/aws-nlb` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../ingress-nginx/aws-nlb
```
