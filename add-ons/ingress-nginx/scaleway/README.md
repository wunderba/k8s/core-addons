# ingress-nginx/scaleway add-on

Install [Ingress NGINX Controller](https://github.com/kubernetes/ingress-nginx) and configure it
for Scaleway.

This will add the 

```yaml
service.beta.kubernetes.io/scw-loadbalancer-proxy-protocol-v2: "true"
service.beta.kubernetes.io/scw-loadbalancer-use-hostname: "true"
```

annotations to the ingress `Service` object and configure ingress-nginx for using the
proxy_protocol in talking to the load balancer:

```yaml
config:
  use-proxy-protocol: "true"
```

## Usage

Simply include `ingress-nginx/scaleway` in your add-ons config.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../ingress-nginx/scaleway
```
