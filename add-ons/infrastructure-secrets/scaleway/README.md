# infrastructure-secrets/scaleway add-on

External secrets configuration for specific circumstances using Scaleway Secret Manager

## Usage

Simply include `external-secrets` and `infrastructure-secrets` in your add-ons config.


```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../external-secrets/scaleway
    - ../infrastructure-secrets/scaleway
...
```
