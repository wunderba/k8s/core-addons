# external-secrets/scaleway add-on

Install [External Secrets Operator](https://external-secrets.io) and connect it
to Scaleway Secret Manager

## Usage

Simply include `external-secrets/scaleway` in your add-ons config.

This will configure a `ClusterSecretStore` named `scaleway-${default_region}` for the
specified region and the `${default_project_id}` taken from you `cluster-vars` ConfigMap.

```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../external-secrets/scaleway
```
