# external-secrets/generic add-on

Install [External Secrets Operator](https://external-secrets.io) in your cluster.

[[_TOC_]]

## Usage

Include `external-secrets` in your add-ons config.

This will just install External Secrets Operator, but not configure any CRs for
a specific secrets store. To immediately connect to a secrets store, use one
of the following variants:

Currently only

* [scaleway](../scaleway/)


```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: core-addons
  namespace: flux-system
spec:
  interval: 50m
  path: ./add-ons/core-sources

  components:
    - ../external-secrets/generic
```
