# external-secrets add-on

Install [External Secrets Operator](https://external-secrets.io) in your cluster.

## Variants

* [external-secrets/generic](generic)
* [external-secrets/scaleway](scaleway)
